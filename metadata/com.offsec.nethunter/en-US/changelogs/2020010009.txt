* Add background location support for Android 10
* Considerable re-write by @simonpunk
* KeX manager by @yesimxev
* usbarmory script by @simonpunk
* Create symlink "kalifs" for chroot directory to allow switching chroots on the fly
