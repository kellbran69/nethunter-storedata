v4.1.0
- Option to disable cursor
- Fixes to cursor shape handling
- Fix for devices with cutouts
- Fix for back button right click
- Fix for pointer bad pointer location when pointer shape changes
v4.0.9
- Japanese, Korean, Traditional Chinese, and Simplified Chinese localizations
v4.0.8
- Security updates
v4.0.7
- 64-bit support
- Bugfixes
v4.0.6
- Bugfix for TLS_FALLBACK_SCSV issue with TLSv1.3+ for VeNCrypt
- Bugfix to sshlib for sha2-256 and sha2-512 keys
- Support for shorthand VNC port n
v4.0.4
- Fix for scrolling in Single Handed mode
v4.0.3
- Updated sshlib
v4.0.2
- Fixes for widget names
- Fixes for Fit-to-screen scaling
- Default Input Mode support
v4.0.1
- Text contrast fix for certain devices
- Targeting new APIs
- Fix for Menu + immersive mode live-lock on some devices
v4.0.0
- Input mode bugfixes
- Security and protocol library updates
v3.9.9
- Fix for IMEs that use META_SHIFT_ON
- Fix for right-click in TouchPad mode
- ExtendedDesktopSize support for TigerVNC
v3.9.7
- 
